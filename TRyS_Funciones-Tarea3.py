def cambiar_data(packet):
    try:
        if packet["IP"]["proto"] == 6:
            if packet["DATA"]["data"].decode('UTF-8') != "":
                print(packet["DATA"]["data"])
                packet["DATA"]["data"] = str("hola").encode("UTF-8")
                print(packet["DATA"]["data"])
                return packet
    except:
        return packet

def cambiar_largo(packet):
    if packet['IP']['src'] == '172.17.0.1':
        print(packet['TCP']['len'])
        packet['TCP']['len'] = "0x00"
        print(packet['TCP']['len'])
        return packet

def cambiar_protocolo(packet):
    if packet['IP']['src'] == '172.17.0.1':
        print(packet['IP']['proto'])
        packet['IP']['proto'] = 3 #9
        print(packet['IP']['proto'])
        return packet
