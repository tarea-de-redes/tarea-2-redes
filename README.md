# Tarea 2 Redes

## Nombre
Taller de Redes y Servicios - Tarea 2

## Descripcion
Se investigo el comportamiento del protocolo Minecraft, utilizando el servidor de minecraft y WebMC.

Se analizaron los paquetes provenientes de estos con Wireshark, investigando las etapas que sigue el protocolo y sus patrones.

## Video

https://www.youtube.com/watch?v=eWckxQsxbS0


# Tarea 3 Redes

## Descripcion
Realizamos la modificación e inyección de paquetes entre un servidor y un cliente de Minecraft.

Las modificaciones fueron realizadas utilizando Polymorph dentro de un Docker, estas fueron un cambio al largo de TCP, un cambio de Protocolo y un cambio de DATA.

La inyección fue realizada utilizando Scapy, realizando un fuzzing tanto al cliente como el servidor modificando los campos para que estos parecieran ser enviados entre ellos.

## Video

https://youtu.be/JL9uNwekhb4

# Tarea 4 Redes


## Descripcion
En el presente video se analizo el comportamiento del protocolo a través de las herramientas de netem y polymorph, primero se aplico la métrica de packet loss a un 25% el cual tuvo las repercusiones esperadas en el método offline en los paquetes de wireshark, aumentando el tiempo de los paquetes RTT y para el caso del método en tiempo real  se utilizo una funcion en polymorph que calcula el jitter y nos muestra cuanto es el tiempo de este en cada paquete, al principio sin aplicar la metrica se evidencio que este era normal y al aplicarlo no se evidenciaron cambios y este se mantuvo normal dado que no fue afectado por la metrica de perdida de paquetes. 

Como segunda metrica se utilizo la de delay que aplicaba un delay de 3000ms el cual tuvo repercuciones esperadas a nivel de relentizar los procesos y para el caso del método en tiempo real  se utilizo una funcion en polymorph que calcula el throughput y nos muestra cuanto es el tiempo de este en cada paquete, al principio sin aplicar la metrica se evidencio que este era alto pero una vez aplicada empezo a disminuir lo que se dio a entender que esta si tenia un efecto en el throughput.

## Video

https://youtu.be/TtvxziislJM}{https://youtu.be/TtvxziislJM
